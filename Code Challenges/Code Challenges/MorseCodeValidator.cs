﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Challenges
{
    public static class MorseCodeValidator
    {
        private const int WordSpacing = 7;
        private const int CharacterSpacing = 3;

        // I know there's a better way to do this, and it kills me that I can't think of one.
        // Maybe I should have leanred Hoffman coding?

        private static readonly Dictionary<string,string> MorseMapping = new Dictionary<string, string>
        {
            {"A","· —"},
            {"B","— · · ·"},
            {"C","— · — ·"},
            {"D","— · ·"},
            {"E","·"},
            {"F","· · — ·"},
            {"G","— — ·"},
            {"H","· · · ·"},
            {"I","· ·"},
            {"J","· — — —"},
            {"K","— · —"},
            {"L","· — · ·"},
            {"M","— —"},
            {"N","— ·"},
            {"O","— — —"},
            {"P","· — — ·"},
            {"Q","— — · —"},
            {"R","· — ·"},
            {"S","· · ·"},
            {"T","—"},
            {"U","· · —"},
            {"V","· · · —"},
            {"W","· — —"},
            {"X","— · · —"},
            {"Y","— · — —"},
            {"Z","— — · ·"},
            {"1","· — — — —"},
            {"2","· · — — —"},
            {"3","· · · — —"},
            {"4","· · · · —"},
            {"5","· · · · ·"},
            {"6","— · · · ·"},
            {"7","— — · · ·"},
            {"8","— — — · ·"},
            {"9","— — — — ·"},
            {"0","— — — — —"}
        };

        public static MorseCode ConvertTextToMorse(string inputText)
        {

            var pieces = inputText.ToUpperInvariant().Split(' ');
            var code = string.Empty;
            foreach (var piece in pieces)
            {
                code += MorseMapping.Aggregate(piece, (current, value) => 
                                                            current.Replace(value.Key, value.Value + new string(' ', CharacterSpacing))).TrimEnd(' ');
                code += new string(' ', WordSpacing);
            }
            return new MorseCode(code);
        }
    }

    public class MorseCode
    {

        private const int UnitOfTimeInMiliseconds = 240;
        private const int FrequencyOfNoteInHertz = 750;

        public MorseCode(string code)
        {
            Code = code;
        }

        public string Code { get; private set; }

        public int Duration
        {
            get
            {
                return Code.Length + Code.Count(c => c == '—') * 2;
            }
        }

        public void Play()
        {
            foreach (var bit in Code)
            {
                switch (bit)
                {
                    case ' ':
                        // Please forgive me. This is terribly low level.
                        Thread.Sleep(UnitOfTimeInMiliseconds);
                        break;
                    case '—':
                        Console.Beep(FrequencyOfNoteInHertz, UnitOfTimeInMiliseconds * 3);
                        break;
                    case '·':
                        Console.Beep(FrequencyOfNoteInHertz, UnitOfTimeInMiliseconds);
                        break;
                }
            }
        }

        public override string ToString()
        {
            return string.Format("{0}{1}Duration: {2}", Code, Environment.NewLine, Duration);
        }
    }
}
