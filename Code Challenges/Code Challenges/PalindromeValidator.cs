﻿using System.Linq;

namespace Challenges
{
    public static class PalindromeValidator
    {
        public static bool IsPalindrome(string candidate)
        {
            // Strip candidate of punctuation and other impurities
            var purifiedCandidate = new string(candidate.Where(c => char.IsLetterOrDigit(c)).ToArray()).ToLowerInvariant();

            var maxIndex = purifiedCandidate.Length - 1;
            var forwardIndex = 0;
            var backwardsIndex = maxIndex;

            while (forwardIndex <= backwardsIndex)
            {
                if(!purifiedCandidate[forwardIndex++].Equals(purifiedCandidate[backwardsIndex--]))
                    return false;
            }

            return true;
        }
    }
}
