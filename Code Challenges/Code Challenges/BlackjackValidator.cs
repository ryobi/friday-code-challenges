﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenges
{
    public class BlackjackValidator
    {
        private const int FaceCardValue = 10;
        private const int AceHighValue = 11;
        private const int AceLowValue = 1;

        public static int BestCaseFromHand(IEnumerable<string> hand)
        {
            
            int result = 0;
            IList<string> nonNumericalCards = new List<string>(); 

            // Find all numbers in hand and add them up
            foreach (var card in hand)
            {
                int handValue;
                if (int.TryParse(card, out handValue))
                {
                    result += handValue;
                }
                else
                {
                    nonNumericalCards.Add(card);
                }
            }

            //Add all values except Aces
            int acesInHand = nonNumericalCards.Count(card => card.Equals("A"));
            result += (nonNumericalCards.Count - acesInHand) * FaceCardValue;

            // Deal with aces
            for (int aces = 0; aces < acesInHand; aces++)
            {
                if (result + AceHighValue > 21)
                    result += AceLowValue;
                else
                    result += AceHighValue;
            }

            return result;
        }
    }
}
