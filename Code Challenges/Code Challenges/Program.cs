﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Challenges
{
    class Program
    {

        static readonly Dictionary<string, Action> Challenges = new Dictionary<string, Action>
        {
            {"Palindrome", PalindromeInput},
            {"Blackjack", BlackjackInput},
            {"Morse Code", MorseCodeInput},
            {"Audible Morse Code", AudibleMorseCodeInput},

        };

        static void Main(string[] args)
        {
            int inputParsed;
            
            if (args.Any() && int.TryParse(args.FirstOrDefault(), out inputParsed) && inputParsed >= 0 && inputParsed < Challenges.Count)
            {
                Challenges.Values.ElementAt(inputParsed)();
                return;
            }

            for (int i = 0; i < Challenges.Count; i++)
            {
                Console.WriteLine("{0} - {1}",i,Challenges.Keys.ElementAt(i));
            }
            
            Console.WriteLine("{0}Select a challenge: ",Environment.NewLine);
            var input = Console.ReadLine();

            if (int.TryParse(input, out inputParsed) && inputParsed >= 0 && inputParsed < Challenges.Count)
            {
                Challenges.Values.ElementAt(inputParsed)();
            }

            Console.ReadLine();
        }

        private static void PalindromeInput()
        {
            Console.Write("Enter a palindrome candidate: ");
            var candidate = Console.ReadLine();
            var result = PalindromeValidator.IsPalindrome(candidate);
            Console.WriteLine("The candidate {0} is{1} a palindrome", candidate, (result) ? "" : " not");
        }

        private static void BlackjackInput()
        {
            Console.Write("Enter a blackjack hand separated by comma, no spaces: ");
            var candidate = Console.ReadLine();
            if (candidate != null)
            {
                var result = BlackjackValidator.BestCaseFromHand(candidate.Split(','));
                Console.WriteLine("The best hand from {0} is {1}", candidate, result);
            }
        }

        private static void MorseCodeInput()
        {
            Console.Write("Enter text to be converted: ");
            var candidate = Console.ReadLine();
            if (!string.IsNullOrWhiteSpace(candidate))
            {
                var result = MorseCodeValidator.ConvertTextToMorse(candidate);
                Console.WriteLine("{0}",result);
            }
        }

        private static void AudibleMorseCodeInput()
        {
            Console.Write("Enter text to be converted: ");
            var candidate = Console.ReadLine();
            if (!string.IsNullOrWhiteSpace(candidate))
            {
                var result = MorseCodeValidator.ConvertTextToMorse(candidate);
                Console.WriteLine("{0}", result);
                result.Play();
            }
        }
    }
}
