﻿using System.Collections.Generic;
using NUnit.Framework;

namespace Challenges.Tests
{
    [TestFixture]
    public class MorseCodeTests
    {
        static readonly IDictionary<string, MorseCode> ExpectedResults = new Dictionary<string, MorseCode>
        {
            {"SOS", new MorseCode("· · ·   — — —   · · ·       ")},
            {"HI PALS", new MorseCode("· · · ·   · ·       · — — ·   · —   · — · ·   · · ·       ")},
            {"PARIS", new MorseCode("· — — ·   · —   · — ·   · ·   · · ·       ")},
            {"123", new MorseCode("· — — — —   · · — — —   · · · — —       ")},
            {"Fire on the 3rd hill", new MorseCode("· · — ·   · ·   · — ·   ·       — — —   — ·       —   · · · ·   ·       · · · — —   · — ·   — · ·       · · · ·   · ·   · — · ·   · — · ·       ")}
        };

        static readonly IDictionary<string, int> KnownDuration = new Dictionary<string, int>
        {
            {"SOS", 34},
            {"PARIS", 50},
        };

        [Test, TestCaseSource("ExpectedResults")]
        public void MorseCode_InputTextString_ReturnsExpectedCode(KeyValuePair<string, MorseCode> candidate)
        {
            // Arrange

            // Act
            var result = MorseCodeValidator.ConvertTextToMorse(candidate.Key);

            // Assert
            Assert.AreEqual(candidate.Value.Code, result.Code);
        }

        [Test, TestCaseSource("KnownDuration")]
        public void MorseCode_InputTextString_ReturnsExpectedDuration(KeyValuePair<string, int> candidate)
        {
            // Arrange

            // Act
            var result = MorseCodeValidator.ConvertTextToMorse(candidate.Key);

            // Assert
           Assert.AreEqual(candidate.Value, result.Duration);
        }

    }
}
