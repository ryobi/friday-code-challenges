﻿using System.Collections.Generic;
using NUnit.Framework;

namespace Challenges.Tests
{
    [TestFixture]
    public class BlackjackTests
    {
        static readonly IDictionary<IEnumerable<string>, int> ExpectedResults = new Dictionary<IEnumerable<string>, int>()
        {
            {new[] {"A", "J"}, 21},
            {new[] {"A"}, 11},
            {new[] {"A", "J", "3"}, 14},
            {new[] {"5", "8"}, 13},
            {new[] {"A", "J", "K", "3"}, 24},
            {new[] {"J", "K", "3"}, 23},
        };

        static readonly IDictionary<IEnumerable<string>, int> IncorrectResults = new Dictionary<IEnumerable<string>, int>()
        {
            {new[] {"A", "J"}, 5},
            {new[] {"A"}, 1},
            {new[] {"A", "A", "3"}, 25},
            {new[] {"5", "8"}, 7},
            {new[] {"A", "J", "K", "3"}, 21},
            {new[] {"A"}, 0}
        };

        [Test, TestCaseSource("ExpectedResults")]
        public void Blackjack_ValidateBestHand_ReturnsExpectedResult(KeyValuePair<IEnumerable<string>, int> candidate)
        {
            // Arrange

            // Act
            var result = BlackjackValidator.BestCaseFromHand(candidate.Key);

            // Assert
            Assert.AreEqual(candidate.Value, result);
        }

        [Test, TestCaseSource("IncorrectResults")]
        public void Blackjack_ValidateIncorrectHand_ReturnsUnexpectedResult(KeyValuePair<IEnumerable<string>, int> candidate)
        {
            // Arrange

            // Act
            var result = BlackjackValidator.BestCaseFromHand(candidate.Key); ;

            // Assert
            Assert.AreNotEqual(candidate.Value, result);
        }
    }
}
