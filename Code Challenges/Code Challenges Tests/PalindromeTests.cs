﻿using NUnit.Framework;

namespace Challenges.Tests
{
    [TestFixture]
    public class PalindromeTests
    {
        static string[] ActualPalindromes = { "Amore, Roma", "A man, a plan, a canal: Panama", "No 'x' in 'Nixon'" };
        static string[] FalsePalindromes = { "'Yer a wizard, Frodo' - Darth Vader" };

        [Test, TestCaseSource("ActualPalindromes")]
        public void Palindromes_ValidateActualPalindromes_ReturnsTrue(string candidate)
        {
            // Arrange

            // Act
            var result = PalindromeValidator.IsPalindrome(candidate);

            // Assert
            Assert.IsTrue(result);
        }

        [Test, TestCaseSource("FalsePalindromes")]
        public void Palindromes_ValidateFalsePalindromes_ReturnsFalse(string candidate)
        {
            // Arrange

            // Act
            var result = PalindromeValidator.IsPalindrome(candidate);

            // Assert
            Assert.IsFalse(result);
        }
    }
}
